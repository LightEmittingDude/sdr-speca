# #########################################################
# SDRSpecA.py
#
# This program implements a spectrum analyzer using the
# RTL-SDR dongle
#
# Author: Kasun Somaratne
# Date: May 27, 2018
# #########################################################

from tkinter import *
from tkinter.ttk import *
from tkinter import simpledialog
from tkinter import messagebox
from tkinter import Checkbutton
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import matplotlib.ticker as ticker
import warnings
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from scipy import signal
from math import log10, ceil
from rtlsdr import *

# suppress deprecated warnings
warnings.filterwarnings('ignore')

# device specs
# max sample rate = 2.4e6

class Application(Frame):
    """Implements the spectrum analyzer application"""
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack(side='top', fill='both', expand=True)
        self.master.protocol('WM_DELETE_WINDOW', self.mainWindowOnClose)
        
        self.maxHoldData = []
        self.minHoldData = []
        self.averageData = []
        
        # spectrum analyzer default settings
        self.center_freq = 410.7875 # MHz
        self.span = 2 # MHz
        self.numsamples = 4096
        self.avgsamples = 8
        self.rbw = 0.5 # kHz
        self.ampMax = -20
        self.ampMin = -100
        self.spanMin = self.center_freq - self.span / 2
        self.spanMax = self.center_freq + self.span / 2 
        
        # configure SDR
        self.sdr = RtlSdr()
        self.sdr.sample_rate = 2.048e6 # Hz
        self.sdr.center_freq = self.center_freq * 1e6 # Hz
        self.sdr.gain = 'auto'
        
        self.createMainWindow()
        self.animation = ani.FuncAnimation(self.fig, self.animate, repeat=False, interval=10)   
        
    def createMainWindow(self):
        """creates the main application window"""
        # create embedded plot window
        self.fig = Figure(figsize=(10,5), dpi=100)
        self.ax1 = self.fig.add_subplot(111)
        
        plotFrame = Frame(self)
        plotFrame.grid(row=0, column=0, rowspan=2, padx=5, pady=5, sticky='n')
        
        plotCanvas = FigureCanvasTkAgg(self.fig, plotFrame)
        plotCanvas.draw()
        plotCanvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)
        
        plotToolbar = NavigationToolbar2TkAgg(plotCanvas, plotFrame)
        plotToolbar.update()
        plotCanvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=True)
        
        # Spectrum Analyzer Controls
        controlFrame = LabelFrame(self, text='Display', relief='groove')
        controlFrame.grid(row=0, column=1, padx=5, pady=5, sticky='n')
        
        Label(controlFrame, text='Freq. mode: ').grid(row=0, column=0, padx=5, pady=5, sticky='e')
        self.fModeButVar = StringVar()
        self.fModeButVar.set('Centre/Span')
        self.fModeButton = Button(controlFrame, textvariable=self.fModeButVar, command=self.setFreqMode)
        self.fModeButton.grid(row=0, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='Freq. center (MHz): ').grid(row=1, column=0, padx=5, pady=5, sticky='e')
        self.fcButVar = StringVar()
        self.fcButVar.set(str(self.center_freq))
        self.fcButton = Button(controlFrame, textvariable=self.fcButVar, command=self.setFreqCentre)
        self.fcButton.grid(row=1, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='Freq. start (MHz): ').grid(row=2, column=0, padx=5, pady=5, sticky='e')
        self.fStartButVar = StringVar()
        self.fStartButVar.set(str(self.spanMin))
        self.fStartButton = Button(controlFrame, textvariable=self.fStartButVar, command=self.setFreqStart, state='disabled')
        self.fStartButton.grid(row=2, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='Freq. stop (MHz): ').grid(row=3, column=0, padx=5, pady=5, sticky='e')
        self.fStopButVar = StringVar()
        self.fStopButVar.set(str(self.spanMax))
        self.fStopButton = Button(controlFrame, textvariable=self.fStopButVar, command=self.setFreqStop, state='disabled')
        self.fStopButton.grid(row=3, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='Span (MHz): ').grid(row=4, column=0, padx=5, pady=5, sticky='e')
        self.spanButVar = StringVar()
        self.spanButVar.set(str(self.span))
        self.spanButton = Button(controlFrame, textvariable=self.spanButVar, command=self.setFreqSpan)
        self.spanButton.grid(row=4, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='RBW (kHz): ').grid(row=5, column=0, padx=5, pady=5, sticky='e')
        self.rbwButVar = StringVar()
        self.rbwButVar.set(str(self.rbw))
        self.rbwButton = Button(controlFrame, textvariable=self.rbwButVar, command=self.setRbw)
        self.rbwButton.grid(row=5, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='Amp max (dB/Hz): ').grid(row=6, column=0, padx=5, pady=5, sticky='e')
        self.ampMaxButVar = StringVar()
        self.ampMaxButVar.set(str(self.ampMax))
        self.ampMaxButton = Button(controlFrame, textvariable=self.ampMaxButVar, command=self.setAmpMax)
        self.ampMaxButton.grid(row=6, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='Amp min (dB/Hz): ').grid(row=7, column=0, padx=5, pady=5, sticky='e')
        self.ampMinButVar = StringVar()
        self.ampMinButVar.set(str(self.ampMin))
        self.ampMinButton = Button(controlFrame, textvariable=self.ampMinButVar, command=self.setAmpMin)
        self.ampMinButton.grid(row=7, column=1, padx=5, pady=5, sticky='w')
        
        Label(controlFrame, text='sample averages: ').grid(row=8, column=0, padx=5, pady=5, sticky='e')
        self.sampleAvgButVar = StringVar()
        self.sampleAvgButVar.set(str(self.avgsamples))
        self.sampleAvgButton = Button(controlFrame, textvariable=self.sampleAvgButVar, command=self.setSampleAvg)
        self.sampleAvgButton.grid(row=8, column=1, padx=5, pady=5, sticky='w')
        
        # button to pause/resume updating the plot
        self.plotCtrlButVar = StringVar()
        self.plotCtrlButVar.set('Pause')
        self.plotCtrlButton = Button(controlFrame, textvariable=self.plotCtrlButVar, command=self.setPlotStatus)
        self.plotCtrlButton.grid(row=9, column=0, columnspan=2, padx=5, pady=5)
        
        # selections for type of plots to display
        plotControlFrame = LabelFrame(self, text='Plots', relief='groove')
        plotControlFrame.grid(row=1, column=1, padx=5, pady=5, sticky='nwes')
        
        self.clearWriteVar = IntVar()
        self.clearWriteVar.set(1)
        Checkbutton(plotControlFrame, text='Clear/Write', variable=self.clearWriteVar, fg='blue').grid(row=0, column=0, padx=5, pady=5, sticky='w')
        
        self.maxHoldVar = IntVar()
        self.maxHoldVar.set(0)
        Checkbutton(plotControlFrame, text='Max hold', variable=self.maxHoldVar, command=self.clearMaxHold, fg='red').grid(row=1, column=0, padx=5, pady=5, sticky='w')
        
        self.minHoldVar = IntVar()
        self.minHoldVar.set(0)
        Checkbutton(plotControlFrame, text='Min hold', variable=self.minHoldVar, command=self.clearMinHold, fg='green').grid(row=2, column=0, padx=5, pady=5, sticky='w')
        
        self.avgPlotVar = IntVar()
        self.avgPlotVar.set(0)
        Checkbutton(plotControlFrame, text='Average', variable=self.avgPlotVar, command=self.clearAverageHold, fg='gold').grid(row=3, column=0, padx=5, pady=5, sticky='w')
        
    def mainWindowOnClose(self):
        """function to call when the main window closes"""
        self.quit()
        
    def clearMaxHold(self):
        """This function clears the max hold array"""
        self.maxHoldData = []
        
    def clearMinHold(self):
        """This function clears the min hold array"""
        self.minHoldData = []
        
    def clearAverageHold(self):
        """This function clears the average hold array"""
        self.averageData = []
        
    def setFreqMode(self):
        """function switches frequency mode between centre/span and start/stop"""
        if self.fModeButVar.get() == 'Centre/Span':
            self.fModeButVar.set('Start/Stop')
            self.fcButton.config(state='disabled')
            self.spanButton.config(state='disabled')
            self.fStartButton.config(state='normal')
            self.fStopButton.config(state='normal')
        else:
            self.fModeButVar.set('Centre/Span')
            self.fcButton.config(state='normal')
            self.spanButton.config(state='normal')
            self.fStartButton.config(state='disabled')
            self.fStopButton.config(state='disabled')
            
    def setFreqStart(self):
        """sets the start frequency of the analyzer"""
        newFreq = simpledialog.askfloat("Start Frequency", "Please enter the start frequency in MHz", parent=self, minvalue=25.0, maxvalue=1750.0)
        
        if newFreq is not None:
            if newFreq > self.spanMax:
                messagebox.showerror('Invalid Frequency', 'Start frequency must be less than stop frequency', parent=self.master)
                return
            self.spanMin = newFreq
            self.fStartButVar.set(str(self.spanMin))
            self.span = self.spanMax - self.spanMin
            self.spanButVar.set("{:.4f}".format(self.span))
            self.center_freq = (self.spanMin + self.spanMax) / 2
            self.fcButVar.set("{:.4f}".format(self.center_freq))
            self.sdr.center_freq = self.center_freq * 1e6
        
    def setFreqStop(self):
        """sets the stop frequency of the analyzer"""
        newFreq = simpledialog.askfloat("Stop Frequency", "Please enter the stop frequency in MHz", parent=self, minvalue=25.0, maxvalue=1750.0)
        
        if newFreq is not None:
            if newFreq < self.spanMin:
                messagebox.showerror('Invalid Frequency', 'Stop frequency must be greater than start frequency', parent=self.master)
                return
            self.spanMax = newFreq
            self.fStopButVar.set(str(self.spanMax))
            self.span = self.spanMax - self.spanMin
            self.spanButVar.set("{:.4f}".format(self.span))
            self.center_freq = (self.spanMin + self.spanMax) / 2
            self.fcButVar.set("{:.4f}".format(self.center_freq))
            self.sdr.center_freq = self.center_freq * 1e6
        
    def setPlotStatus(self):
        """function to control the status of the plot animation"""
        if self.plotCtrlButVar.get() == 'Pause':
            self.animation.event_source.stop()
            self.plotCtrlButVar.set('Resume')
        else:
            self.animation.event_source.start()
            self.plotCtrlButVar.set('Pause')
        
    def setFreqCentre(self):
        """sets the center frequency of the analyzer"""
        newFreq = simpledialog.askfloat("Centre Frequency", "Please enter the centre frequency in MHz", parent=self, minvalue=50.0, maxvalue=1700.0)
        
        if newFreq is not None:
            self.center_freq = newFreq
            self.fcButVar.set(str(self.center_freq))
            self.sdr.center_freq = self.center_freq * 1e6
            self.spanMin = self.center_freq - self.span / 2
            self.fStartButVar.set("{:.4f}".format(self.spanMin))
            self.spanMax = self.center_freq + self.span / 2
            self.fStopButVar.set("{:.4f}".format(self.spanMax))
        
    def setFreqSpan(self):
        """sets the span of the analyzer"""
        newSpan = simpledialog.askfloat("Frequency Span", "Please enter the frequency span in MHz", parent=self, minvalue=0.005, maxvalue=100)
        
        if newSpan is not None:
            self.span = newSpan
            self.spanButVar.set(str(newSpan))
            self.spanMin = self.center_freq - self.span / 2
            self.fStartButVar.set("{:.4f}".format(self.spanMin))
            self.spanMax = self.center_freq + self.span / 2
            self.fStopButVar.set("{:.4f}".format(self.spanMax))
        
    def setRbw(self):
        """sets the resolution bandwidth of the analyzer"""
        newRbw = simpledialog.askfloat("Resolution bandwidth", "Please enter the resolution bandwidth in kHz", parent=self, minvalue=0.1, maxvalue=1000.0)
        
        if newRbw is not None:
            if newRbw > self.span*1e3:
                messagebox.showerror('Invalid RBW', 'Resolution bandwidth cannot exceed span!', parent=self.master)
                return
            # calculate the number of sample points based on the chosen rbw
            self.numsamples = int(round(self.sdr.sample_rate / (newRbw * 1e3)))
            # make the number of samples even
            if not self.numsamples % 2 == 0:
                self.numsamples = self.numsamples + 1
            # update the rbw
            self.rbw = self.sdr.sample_rate / 1e3 / self.numsamples
            self.rbwButVar.set("{:.2f}".format(self.rbw))
            
    def setAmpMax(self):
        """sets the maximum amplitude of the analyzer"""
        newAmpMax = simpledialog.askfloat("Amplitude Max", "Please enter the maximum amplitude in dB/Hz", parent=self, minvalue=-140, maxvalue=30.0)
        
        if newAmpMax is not None:
            if self.ampMin >= newAmpMax:
                messagebox.showerror('Invalid Amplitude', 'Amp Max must be greater than Amp Min', parent=self.master)
                return
            self.ampMax = newAmpMax
            self.ampMaxButVar.set(str(self.ampMax))
        
    def setAmpMin(self):
        """sets the minimum amplitude of the analyzer"""
        newAmpMin = simpledialog.askfloat("Amplitude Min", "Please enter the minimum amplitude in dB/Hz", parent=self, minvalue=-140, maxvalue=30.0)
        
        if newAmpMin is not None:
            if self.ampMax <= newAmpMin:
                messagebox.showerror('Invalid Amplitude', 'Amp Min must be less than Amp Max', parent=self.master)
                return
            self.ampMin = newAmpMin
            self.ampMinButVar.set(str(self.ampMin))
            
    def setSampleAvg(self):
        """sets the number of samples to be averaged"""
        newSampleAvg = simpledialog.askinteger("Sample averages", "Please enter the number of samples to be averaged", parent=self, minvalue=1, maxvalue=100)
        
        if newSampleAvg is not None:
            self.avgsamples = newSampleAvg
            self.sampleAvgButVar.set(str(self.avgsamples))
        
    def animate(self, i):
        """function that gets called repeatedly to display the live spectrum"""
        xData = []
        yData = []
        trimRatio = 0.75 # this is the ratio of the FFT bins taken to remove FFT edge effects 
        requestedFc = self.sdr.center_freq
        # read samples that covers the required frequency span
        self.sdr.center_freq = self.spanMin * 1e6 + (self.sdr.sample_rate * trimRatio) / 2
        while self.sdr.center_freq < (self.spanMax * 1e6 + (self.sdr.sample_rate * trimRatio) / 2):
            # read samples from SDR
            samples = self.sdr.read_samples(self.avgsamples*self.numsamples)
            # calculate power spectral density
            f, pxx = signal.welch(samples, fs=self.sdr.sample_rate, nperseg=self.numsamples)
            # rotate the arrays so the plot values are continuous and also trim the edges
            f = list(f)
            pxx = list(pxx)
            f = f[int(self.numsamples/2 + self.numsamples*(1-trimRatio)/2):] + f[:int(self.numsamples/2 - self.numsamples*(1-trimRatio)/2)]
            pxx = pxx[int(self.numsamples/2 + self.numsamples*(1-trimRatio)/2):] + pxx[:int(self.numsamples/2 - self.numsamples*(1-trimRatio)/2)]
            # adjust the format of the values to be plotted and add to plot arrays
            xData = xData + [(x+self.sdr.center_freq)/1e6 for x in f]
            yData = yData + [10*log10(y) for y in pxx]
            # calculate the next center frequency
            self.sdr.center_freq = self.sdr.center_freq + (self.sdr.sample_rate * trimRatio)
        # reset the sdr center frequency to requested frequency
        self.sdr.center_freq = requestedFc
        # plot the power spectral density
        self.ax1.clear()
        self.ax1.set_ylim(self.ampMin, self.ampMax)
        self.ax1.set_xlim(self.spanMin, self.spanMax)
        self.ax1.xaxis.get_major_formatter().set_useOffset(False)
        self.ax1.set_xlabel('Frequency (MHz)')
        self.ax1.set_ylabel('Spectral Power Density (dB/Hz)')
        if self.clearWriteVar.get():
            self.ax1.plot(xData,yData)
        # if enabled display maxHoldData
        if self.maxHoldVar.get():
            if not len(self.maxHoldData) == len(yData):
                self.maxHoldData = yData
                return
            else:
                for idx in range(0, len(self.maxHoldData)):
                    if yData[idx] > self.maxHoldData[idx]:
                        self.maxHoldData[idx] = yData[idx] 
            self.ax1.plot(xData, self.maxHoldData, color='red')
        # if enabled display minHoldData
        if self.minHoldVar.get():
            if not len(self.minHoldData) == len(yData):
                self.minHoldData = yData
                return
            else:
                for idx in range(0, len(self.minHoldData)):
                    if yData[idx] < self.minHoldData[idx]:
                        self.minHoldData[idx] = yData[idx] 
            self.ax1.plot(xData, self.minHoldData, color='green')
        # if enabled display average data    
        if self.avgPlotVar.get():
            if not len(self.averageData) == len(yData):
                self.averageData = yData
                return
            else:
                for idx in range(0, len(self.averageData)):
                    self.averageData[idx] = (self.averageData[idx] + yData[idx]) / 2 
            self.ax1.plot(xData, self.averageData, color='gold')
        
root = Tk()
root.resizable(width=False, height=False)
app = Application(master=root)
app.master.title('SDR Spectrum Analyzer')
app.mainloop()
try:
    root.destroy()
except TclError:
    pass